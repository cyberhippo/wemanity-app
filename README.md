# Wemanity - Coaching App

## Context

Lorem Ipsum ...

## Installation instructions


### First usage

```bash
# Create a virtualenv
virtualenv -p python3 wemanity_env

# Activate the environment
source wemanity_env/bin/activate

# Install requirements using pip3
pip3 install -r requirements.txt

# Go to the directory
cd coaching_app

# Apply migrations
python3 manage.py migrate

# Run the App
python3 manage.py runserver
```

> App available at [[http://127.0.0.1:8000/]](http://127.0.0.1:8000/)


### Normal usage

```bash
# Activate the environment
source wemanity_env/bin/activate

# Go to the directory
cd coaching_app

# Run the App
python3 manage.py runserver
```

> App available at [[http://127.0.0.1:8000/]](http://127.0.0.1:8000/)
