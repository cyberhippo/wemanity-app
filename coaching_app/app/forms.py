from django import forms

from .models import Document

class add_document_form(forms.Form):
    name        = forms.CharField(max_length = 130)
    email       = forms.EmailField()
    job_title   = forms.CharField(max_length=30)
    bio         = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Document
        fields = ('name', 'email', 'job_title', 'bio')
