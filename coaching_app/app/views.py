from django.shortcuts import render
from django.http import HttpResponseRedirect

from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse

from django.contrib import messages

from .models import Document
from .forms import add_document_form

from django.contrib.auth.decorators import login_required


# ---------------------------------------------------------- #

def index(request):
    return render(request, 'app/index.html')


def form(request):
    if request.method == "POST":
        # Create a reservation form
        form = add_document_form(request.POST or None)

        if form.is_valid():

            # Create the new document object
            document = Document()

            # Get cleaned form content
            document.name       = form.cleaned_data['name']
            document.email      = form.cleaned_data['email']
            document.job_title  = form.cleaned_data['job_title']
            document.bio        = form.cleaned_data['bio']

            # Save the current Rervation
            document.save()

            # Reinstantiate the form to clear up data
            form = add_document_form()

            return HttpResponseRedirect('success')
    else:

        # Create the form object
        form = add_document_form()

    return render(request, 'app/document_form.html', {'form': form})


def success(request):
    return render(request, 'app/new_document_success.html')

@login_required
def write_pdf_view(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="mypdf.pdf"'

    buffer = BytesIO()
    p = canvas.Canvas(buffer)

    # Start writing the PDF here
    p.drawString(100, 100, 'Hello world.')
    # End writing

    p.showPage()
    p.save()

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response
